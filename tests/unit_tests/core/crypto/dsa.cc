/**                                                                                           //
 * Copyright (c) 2015-2018, The Kovri I2P Router Project                                      //
 *                                                                                            //
 * All rights reserved.                                                                       //
 *                                                                                            //
 * Redistribution and use in source and binary forms, with or without modification, are       //
 * permitted provided that the following conditions are met:                                  //
 *                                                                                            //
 * 1. Redistributions of source code must retain the above copyright notice, this list of     //
 *    conditions and the following disclaimer.                                                //
 *                                                                                            //
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list     //
 *    of conditions and the following disclaimer in the documentation and/or other            //
 *    materials provided with the distribution.                                               //
 *                                                                                            //
 * 3. Neither the name of the copyright holder nor the names of its contributors may be       //
 *    used to endorse or promote products derived from this software without specific         //
 *    prior written permission.                                                               //
 *                                                                                            //
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY        //
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF    //
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL     //
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       //
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,               //
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    //
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,          //
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF    //
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               //
 */

#include "tests/unit_tests/main.h"

#include <memory>

#include "core/crypto/rand.h"
#include "core/crypto/signature.h"

BOOST_AUTO_TEST_SUITE(DSASHA1ests)

struct DSAFixture {
  DSAFixture() {
    verifier = std::make_unique<kovri::core::DSAVerifier>(public_key.data());
  }

  std::array<std::uint8_t, crypto::PkLen::DSA> public_key {{
    0x8d, 0x0e, 0xf2, 0xf5, 0x9b, 0x19, 0x19, 0x4c,
    0xad, 0x38, 0x2f, 0xe2, 0x93, 0x9b, 0xce, 0xf1, 
    0x47, 0xb0, 0xe0, 0x8b, 0x7b, 0x75, 0x65, 0xc6,
    0x10, 0xbf, 0x7d, 0x59, 0x38, 0x24, 0x12, 0xa3, 
    0xca, 0x67, 0x8a, 0x64, 0x31, 0x46, 0x88, 0x31,
    0xf3, 0xb8, 0x98, 0xeb, 0x78, 0x9e, 0x69, 0x62, 
    0xc3, 0x84, 0x5b, 0x9a, 0xe7, 0xdb, 0xa1, 0xd1,
    0x57, 0xac, 0x8c, 0xc3, 0xc0, 0xaf, 0xda, 0x79, 
    0x56, 0x82, 0x3f, 0x94, 0x52, 0xe5, 0x52, 0xbd,
    0xf9, 0x4e, 0x36, 0x49, 0xb0, 0x37, 0xbb, 0x6f, 
    0x53, 0x09, 0xb1, 0xec, 0x68, 0xbb, 0x9a, 0xfc,
    0x6d, 0x23, 0xaf, 0xf2, 0xcd, 0xab, 0x06, 0xca, 
    0x37, 0xcb, 0xf2, 0x5c, 0x2c, 0x2e, 0xf0, 0x1a,
    0xac, 0xa9, 0xdd, 0x54, 0x07, 0xbc, 0x30, 0x6b, 
    0x58, 0x94, 0xe3, 0x0a, 0xe0, 0x47, 0xf6, 0xef,
    0xd8, 0x44, 0x03, 0xaa, 0xe3, 0x36, 0x3b, 0xe1
  }};

  std::array<std::uint8_t, crypto::SigLen::DSA> valid_sig {{
    0x6d, 0x93, 0xa6, 0x8c, 0xc5, 0xb2, 0x5b, 0xae,
    0x75, 0x59, 0x54, 0x12, 0xaa, 0xc2, 0x46, 0x5e, 
    0xd7, 0x2b, 0x4a, 0x8d, 0x64, 0x97, 0x75, 0x0e,
    0xe4, 0xca, 0xf8, 0x83, 0xbc, 0x6c, 0x2c, 0x6e, 
    0xff, 0x9f, 0xd2, 0x76, 0xf4, 0xf1, 0x90, 0x26
  }};

  std::array<std::uint8_t, 48> valid_msg {{
    0x07, 0x60, 0x77, 0x6a, 0xfa, 0x11, 0x0f, 0x1a,
    0xca, 0x87, 0x2e, 0x38, 0xb8, 0xf4, 0xf1, 0x6f, 
    0xfe, 0xd6, 0xbb, 0xb2, 0xec, 0xf3, 0x3e, 0xbe,
    0x87, 0xa4, 0xd1, 0xaf, 0xaf, 0x0b, 0xf4, 0xd9, 
    0xb2, 0x54, 0x9c, 0x16, 0x60, 0x1d, 0xd5, 0x36,
    0xb4, 0x18, 0xb0, 0xe5, 0x31, 0xca, 0x5c, 0xa0
  }};

  std::unique_ptr<kovri::core::DSAVerifier> verifier;
};

BOOST_FIXTURE_TEST_CASE(DSASHA1KeyLength, DSAFixture) {
  BOOST_CHECK_EQUAL(verifier->GetPublicKeyLen(), crypto::PkLen::DSA);
}

BOOST_FIXTURE_TEST_CASE(DSASHA1SignatureLength, DSAFixture) {
  BOOST_CHECK_EQUAL(verifier->GetSignatureLen(), crypto::SigLen::DSA);
}

BOOST_FIXTURE_TEST_CASE(DSASHA1VerifyValid, DSAFixture) {
  // check that the signature is valid
  BOOST_CHECK_EQUAL(
      verifier->Verify(valid_msg.data(), valid_msg.size(), valid_sig.data()),
      true);
}

BOOST_FIXTURE_TEST_CASE(DSASHA1VerifyBadSignature, DSAFixture) {
  // introduce errors in both the message and signature
  valid_sig[2] ^= kovri::core::RandInRange32(1, 128);
  // this should fail verification as well
  BOOST_CHECK_EQUAL(
      verifier->Verify(valid_msg.data(), valid_msg.size(), valid_sig.data()),
      false);
}

BOOST_FIXTURE_TEST_CASE(DSASHA1VerifyBadMessage, DSAFixture) {
  // introduce errors in the message
  valid_msg[6] ^= kovri::core::RandInRange32(1, 128);
  // this should fail verification as well
  BOOST_CHECK_EQUAL(
      verifier->Verify(valid_msg.data(), valid_msg.size(), valid_sig.data()),
      false);
}

BOOST_FIXTURE_TEST_CASE(DSASHA1VerifyBadSignatureAndMessage, DSAFixture) {
  // introduce errors in both the message and signature
  valid_msg[6] ^= kovri::core::RandInRange32(1, 128);
  valid_sig[2] ^= kovri::core::RandInRange32(1, 128);
  // this should fail verification as well
  BOOST_CHECK_EQUAL(
      verifier->Verify(valid_msg.data(), valid_msg.size(), valid_sig.data()),
      false);
}

BOOST_AUTO_TEST_SUITE_END()
