/**                                                                                           //
 * Copyright (c) 2013-2018, The Kovri I2P Router Project                                      //
 *                                                                                            //
 * All rights reserved.                                                                       //
 *                                                                                            //
 * Redistribution and use in source and binary forms, with or without modification, are       //
 * permitted provided that the following conditions are met:                                  //
 *                                                                                            //
 * 1. Redistributions of source code must retain the above copyright notice, this list of     //
 *    conditions and the following disclaimer.                                                //
 *                                                                                            //
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list     //
 *    of conditions and the following disclaimer in the documentation and/or other            //
 *    materials provided with the distribution.                                               //
 *                                                                                            //
 * 3. Neither the name of the copyright holder nor the names of its contributors may be       //
 *    used to endorse or promote products derived from this software without specific         //
 *    prior written permission.                                                               //
 *                                                                                            //
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY        //
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF    //
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL     //
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       //
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,               //
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    //
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,          //
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF    //
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               //
 *                                                                                            //
 * Parts of the project are originally copyright (c) 2013-2015 The PurpleI2P Project          //
 */

#include "core/crypto/signature.h"

#include <cryptopp/asn.h>
#include <cryptopp/dsa.h>
#include <cryptopp/integer.h>
#include <cryptopp/misc.h>
#include <cryptopp/naclite.h>
#include <cryptopp/oids.h>
#include <cryptopp/osrng.h>
#include <cryptopp/rsa.h>
#include <cryptopp/secblock.h>

#include <algorithm>
#include <array>
#include <cstdint>
#include <cstring>
#include <memory>
#include <vector>

#include "crypto_const.h"

#include "core/crypto/rand.h"

namespace kovri {
namespace core {

// TODO(anonimal): remove pimpl, see #785

/**
 *
 * DSA
 *
 */

/// @class DSAVerifierImpl
/// @brief DSA verifier implementation
class DSAVerifier::DSAVerifierImpl {
 public:
  DSAVerifierImpl(
      const std::uint8_t* signing_key) {
    m_PublicKey.Initialize(
        dsap, dsaq, dsag, CryptoPP::Integer(signing_key, crypto::PkLen::DSA));
  }

  bool Verify(
      const std::uint8_t* buf,
      std::size_t len,
      const std::uint8_t* signature) const {
    CryptoPP::DSA::Verifier verifier(m_PublicKey);
    return verifier.VerifyMessage(buf, len, signature, crypto::SigLen::DSA);
  }

 private:
  CryptoPP::DSA::PublicKey m_PublicKey;
};

DSAVerifier::DSAVerifier(
    const std::uint8_t* signing_key)
    : m_DSAVerifierPimpl(
          std::make_unique<DSAVerifierImpl>(signing_key)) {}

DSAVerifier::~DSAVerifier() {}

bool DSAVerifier::Verify(
    const std::uint8_t* buf,
    std::size_t len,
    const std::uint8_t* signature) const {
  return m_DSAVerifierPimpl->Verify(buf, len, signature);
}

/**
 *
 * RSARaw
 *
 */

/// @class RSARawVerifier
/// @brief RSA raw verifier base class
template<typename Hash, std::size_t key_length>
class RSARawVerifier {
 public:
  RSARawVerifier(
      const std::uint8_t* signing_key)
      : m_Modulus(signing_key, key_length) {}

  void Update(
      const std::uint8_t* buf,
      std::size_t len) {
    m_Hash.Update(buf, len);
  }

  bool Verify(
      const std::uint8_t* signature) {
    // RSA encryption first
    CryptoPP::Integer encrypted_signature(
        a_exp_b_mod_c(
            CryptoPP::Integer(signature, key_length),
            CryptoPP::Integer(kovri::core::rsae),
            m_Modulus));  // s^e mod n
    std::vector<std::uint8_t> buf(key_length);
    encrypted_signature.Encode(buf.data(), buf.size());
    std::array<std::uint8_t, Hash::DIGESTSIZE> digest {{}};
    m_Hash.Final(digest.data());
    if (buf.size() < Hash::DIGESTSIZE)
      return false;  // Can't verify digest longer than key
    // We assume digest is right aligned, at least for PKCS#1 v1.5 padding
    return CryptoPP::VerifyBufsEqual(
        buf.data() + (buf.size() - Hash::DIGESTSIZE),
        digest.data(),
        Hash::DIGESTSIZE);
}

 private:
  /// @brief RSA modulus 'n'
  CryptoPP::Integer m_Modulus;
  Hash m_Hash;
};

/**
 *
 * RSASHA5124096Raw
 *
 */

/// @class RSASHA5124096RawVerifierImpl
/// @brief RSASHA5124096 verifier implementation
class RSASHA5124096RawVerifier::RSASHA5124096RawVerifierImpl
    : public RSARawVerifier<CryptoPP::SHA512, crypto::PkLen::RSASHA5124096> {
 public:
  RSASHA5124096RawVerifierImpl(
      const std::uint8_t* signing_key)
      : RSARawVerifier<CryptoPP::SHA512, crypto::PkLen::RSASHA5124096>(signing_key) {}
};

RSASHA5124096RawVerifier::RSASHA5124096RawVerifier(
    const std::uint8_t* signing_key)
    : m_RSASHA5124096RawVerifierPimpl(
          std::make_unique<RSASHA5124096RawVerifierImpl>(signing_key)) {}

RSASHA5124096RawVerifier::~RSASHA5124096RawVerifier() {}

void RSASHA5124096RawVerifier::Update(
    const std::uint8_t* buf,
    std::size_t len) {
  m_RSASHA5124096RawVerifierPimpl->Update(buf, len);
}

bool RSASHA5124096RawVerifier::Verify(
    const std::uint8_t* signature) {
  return m_RSASHA5124096RawVerifierPimpl->Verify(signature);
}

/**
 *
 * Ed25519
 *
 */

/// @class Ed25519VerifierImpl
/// @brief Implementation class for the EdDSA Ed25519 verifier
class Ed25519Verifier::Ed25519VerifierImpl
{
 public:
  Ed25519VerifierImpl(const std::uint8_t* pk) : m_Pk(pk, crypto::PkLen::Ed25519)
  {
  }

  bool Verify(
      const std::uint8_t* m,
      const std::size_t mlen,
      const std::uint8_t* sig) const
  {
    // Combine message with given signature
    CryptoPP::SecByteBlock sm(crypto::SigLen::Ed25519 + mlen);
    std::copy(sig, sig + crypto::SigLen::Ed25519, sm.begin());
    std::copy(m, m + mlen, sm.begin() + crypto::SigLen::Ed25519);

    // Verify
    CryptoPP::SecByteBlock rm(mlen + crypto::SigLen::Ed25519);
    CryptoPP::word64 rmlen;

    int const ret(CryptoPP::NaCl::crypto_sign_open(
        rm, &rmlen, sm.data(), sm.size(), m_Pk.data()));

    return !ret;
  }

 private:
  CryptoPP::SecByteBlock m_Pk;
};

Ed25519Verifier::Ed25519Verifier(const std::uint8_t* pk)
    : m_Ed25519VerifierPimpl(std::make_unique<Ed25519VerifierImpl>(pk))
{
}

Ed25519Verifier::~Ed25519Verifier() {}

bool Ed25519Verifier::Verify(
    const std::uint8_t* m,
    const std::size_t mlen,
    const std::uint8_t* sig) const
{
  return m_Ed25519VerifierPimpl->Verify(m, mlen, sig);
}

/// @class Ed25519SignerImpl
/// @brief Implementation class for the EdDSA Ed25519 signer
class Ed25519Signer::Ed25519SignerImpl
{
 public:
  Ed25519SignerImpl(const std::uint8_t* sk, const std::uint8_t* pk)
      : m_Sk(sk, crypto::SkLen::Ed25519), m_Pk(pk, crypto::PkLen::Ed25519)
  {
  }

  Ed25519SignerImpl(const std::uint8_t* sk)
      : m_Sk(sk, crypto::SkLen::Ed25519), m_Pk(crypto::PkLen::Ed25519)
  {
    // Create keypair
    if (CryptoPP::NaCl::crypto_sign_sk2pk(m_Pk.data(), m_Sk.data()))
      throw CryptoPP::Exception(
          CryptoPP::Exception::OTHER_ERROR, "could not create ed25519 keypair");

    // Concat pubkey with secret key (an I2P'ism)
    std::copy(m_Pk.begin(), m_Pk.end(), m_Sk.end() - 32);
  }

  void Sign(
      const std::uint8_t* m,
      const std::size_t mlen,
      std::uint8_t* signature) const
  {
    // Signed message length
    CryptoPP::word64 smlen;

    // Sign message
    std::vector<std::uint8_t> sm(crypto::SigLen::Ed25519 + mlen);
    if (CryptoPP::NaCl::crypto_sign(sm.data(), &smlen, m, mlen, m_Sk.data()))
      throw CryptoPP::Exception(
          CryptoPP::Exception::OTHER_ERROR, "could not ed25519 sign message");

    // We only want the signature
    std::copy(sm.begin(), sm.end() - mlen, signature);
  }

 private:
  CryptoPP::SecByteBlock m_Sk;  ///< Private key
  CryptoPP::SecByteBlock m_Pk;  ///< Public key
};

Ed25519Signer::Ed25519Signer(const std::uint8_t* sk)
    : m_Ed25519SignerPimpl(std::make_unique<Ed25519SignerImpl>(sk))
{
}

Ed25519Signer::Ed25519Signer(const std::uint8_t* sk, const std::uint8_t* pk)
    : m_Ed25519SignerPimpl(std::make_unique<Ed25519SignerImpl>(sk, pk))
{
}

Ed25519Signer::~Ed25519Signer() {}

void Ed25519Signer::Sign(
    const std::uint8_t* m,
    const std::size_t mlen,
    std::uint8_t* sig) const
{
  m_Ed25519SignerPimpl->Sign(m, mlen, sig);
}

void CreateEd25519KeyPair(std::uint8_t* sk, std::uint8_t* pk)
{
  if (CryptoPP::NaCl::crypto_sign_keypair(pk, sk))
    throw CryptoPP::Exception(
        CryptoPP::Exception::OTHER_ERROR, "could not create ed25519 keypair");
}

}  // namespace core
}  // namespace kovri
